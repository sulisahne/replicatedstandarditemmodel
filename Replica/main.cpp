#include <QAbstractItemModelReplica>
#include <QGuiApplication>
#include <QMetaMethod>
#include <QMetaProperty>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QRemoteObjectNode>
#include <QRemoteObjectPendingCall>
#include <QtDebug>
#include <memory>

void request(const QAbstractItemModelReplica& model, const QModelIndex& parentIndex = {})
{
    int rowCount = model.rowCount(parentIndex);
    int colCount = model.columnCount(parentIndex);

    if (rowCount == 0 && colCount == 0) {
        qDebug() << "empty";
        return;
    }

    for (int row = 0; row != rowCount; row++) {
        for (int col = 0; col != colCount; col++) {
            QModelIndex idx = model.index(row, col, parentIndex);
            model.data(idx, Qt::UserRole + 1);
        }
    }
}

QVector<QModelIndex> onDataChanged(QAbstractItemModelReplica& model, const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles)
{
    QVector<QModelIndex> indices;
    QModelIndex parentIndex = topLeft.parent();

    for (int row = topLeft.row(); row <= bottomRight.row(); row++) {
        for (int col = topLeft.column(); col <= bottomRight.column(); col++) {
            for (int role : roles) {
                QModelIndex idx = model.index(row, col, parentIndex);
                QVariant value = model.data(idx, role);
                qDebug() << value.toString();
                indices.append(idx);
            }
        }
    }

    return indices;
}

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    QRemoteObjectNode node;
    node.setRegistryUrl(QUrl("local:server"));

    auto model = node.acquireModel("MyModel", QtRemoteObjects::FetchRootSize);
    QObject::connect(model, &QAbstractItemModelReplica::initialized,
        [model]() {
            QObject::connect(model, &QAbstractItemModelReplica::dataChanged,
                [model](const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
                    auto indices = onDataChanged(*model, topLeft, bottomRight, roles);
                    for (const auto& index : indices)
                        request(*model, index);
                });

            request(*model);
        });

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    bool result = app.exec();

    delete model;

    return result;
}
