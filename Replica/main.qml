import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import QtRemoteObjects 5.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("ReplicatedStandardItemModel")
}
