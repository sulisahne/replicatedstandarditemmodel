#include "mymodel.h"
#include <QCoreApplication>
#include <QRemoteObjectRegistryHost>

void print(const MyModel& model, const QModelIndex& parentIndex)
{
    for (int row = 0; row != model.rowCount(parentIndex); row++) {
        for (int col = 0; col !=  model.columnCount(parentIndex); col++) {
            QModelIndex idx = model.index(row, col, parentIndex);
            QVariant val = model.data(idx, MyModel::ValueRole);
            qDebug() << val.toString();
            print(model, idx);
        }
    }
}

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);
    QRemoteObjectRegistryHost regHost(QUrl("local:registry"));
    QRemoteObjectHost host(QUrl("local:server"), QUrl("local:registry"));

    MyModel model;

    print(model, {});

    if (!host.enableRemoting(&model, "MyModel", model.roles()))
        qFatal("Could not enable remoting for MyModel");

    qDebug() << "Waiting to connect..";

    auto result = app.exec();

    if (!host.disableRemoting(&model))
        qFatal("Could not disable remoting for MyModel");

    return result;
}
