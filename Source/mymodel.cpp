#include "MyModel.h"
#include "QtDebug"

MyModel::MyModel(QObject* parent)
    : QStandardItemModel(parent)
{
    auto* root1 = new QStandardItem();
    root1->setData(QString("root1"), MyModel::ValueRole);

    auto* root2 = new QStandardItem();
    root2->setData(QString("root2"), MyModel::ValueRole);

    QList<QStandardItem*> entities1;
    for (int i = 0; i < 40; ++i) {
        auto* item = new QStandardItem();
        item->setData(QString("root1 - %0").arg(i), MyModel::ValueRole);
        entities1.append(item);
    }
    root1->appendColumn(entities1);

    QList<QStandardItem*> entities2;
    for (int i = 0; i < 5; ++i) {
        auto* item = new QStandardItem();
        item->setData(QString("root2 - %0").arg(i), MyModel::ValueRole);
        entities2.append(item);
    }
    root2->appendColumn(entities2);

    auto* parentItem = invisibleRootItem();
    parentItem->appendColumn({ root1, root2 });
}

int MyModel::rowCount(const QModelIndex& parent) const
{
    int r = QStandardItemModel::rowCount(parent);
    //qDebug() << "rowCount " << r;
    return r;
}

int MyModel::columnCount(const QModelIndex& parent) const
{
    int c = QStandardItemModel::columnCount(parent);
    //qDebug() << "columnCount " << c;
    return c;
}

QVariant MyModel::data(const QModelIndex& index, int role) const
{
    auto val = QStandardItemModel::data(index, role);
    //qDebug() << "data " << val.toString() << index;
    return val;
}

QHash<int, QByteArray> MyModel::roleNames() const
{
    return {
        { MyModel::ValueRole, "value" }
    };
}

QVector<int> MyModel::roles() const
{
    return {
        MyModel::ValueRole
    };
}
