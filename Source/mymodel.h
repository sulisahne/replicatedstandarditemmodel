#pragma once

#include <QStandardItemModel>

class MyModel : public QStandardItemModel {
    Q_OBJECT

public:
    enum MyModelRoles {
        ValueRole = Qt::UserRole + 1,
    };
    Q_ENUM(MyModelRoles)

    explicit MyModel(QObject* parent = nullptr);
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVector<int> roles() const;
};
